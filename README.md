# Automated Auctions

**This system is now fully controlled by the steambot.**



### Steambot [Timing]
The steambot will **automatically** start the auctions at different times based on the day of the week.

**Auction starting times**
* **Monday** (8PM)
* **Tuesday** (8PM)
* **Wednesday** (8PM)
* **Thursday** (8pm)
* **Friday** (1pm)
* **Saturday** (1pm)
* **Sunday** (8pm)

>Timezone GMT+8


### Steambot [Item filter]
The bot will create between **10** and **12** auctions automatically. **(This value is randomized)**

The items the bot select are defined by the **Filter** as seen below.

**Item Filter**

* **Ward** (1)
* **Misc** (1)
* **Bundle** (7 or 8)
* **Courier** (1 or 2)

**Notes**
* The bot will **not** select two of the same item.
* The bot shuffles its inventory on each new auction.
* The bot will **not** select a item over the price of **$2.50** for a regular auction.
* The bot will **not** allow a regular auction to be higher then **$5.00** total.


### Steambot [Special Auctions]
The bot will do **3** "Special" auctions per week. (**The days of these auctions are randomized.**)


* **Mystery** (1 random item)
* **Mystery Bundle** (2 or more random items)
* **Pandora** (10 random items) **User only gets 1**

**Special Auctions are always the last auction of the day.**
>Please refer to **Auctionlist** to learn how items are selected.

# Auctionlist Usage

To add items into the **Filter** for **Special Auctions** you must do the following.

``$auctionlist [type] [item name] [itemid(s)]``

>The bot deletes the item after it has been selected.

### Auctionlist [Type]

This represents the auction filter you want this item to be added to.

**Accepted Values**
* **Pandora** 
* **Mystery**
* **mbundle** (Mystery Bundle)

>The bot will select a random item based on the **Special Auction** that just finished 

### Auctionlist [item name]

This represents the name of the item.

If you require a space in the name then you must wrap it inside double quotes `"example here"`

### Auctionlist [itemid(s)]

This should include the **steam** assetid(s) for each item you wish the bot to send to the winner of the auction.

If you are supplying more then 1 assetid then you must separate them with ``,`` Example: ``12345,678910``
>Please refer to [steamerrors.com](https://steamerrors.com/tradeoffers) for a list of error response codes.


# Pandora Auction
### How it works. 
Upon a "Pandora" auction finishing the bot will mention the winner from ``#pandora-winners`` asking for a number between 1 & 10 it also allows ``SEND_MESSAGES`` for the winner during this time.

Once the winner replys with a valid value the bot will select **10** random items from the Pandora filter.

The bot will alert them that they won ``X`` item followed by posting the Pandora box embed and removing their ``SEND_MESSAGES`` permission.

> The bot will match the number they provided to the value of the item inside this pandora box then send them a steam offer.

(**<a href="https://i-have-no-social.life/u/5l7Yq0.png">PICTURE</a>**)

**Notes**

* The bot will **only** accept input from the winner and ignores everybody else.
* The winner must supply a value within **3** minutes after this time they must contact support.
* The item the user picked is **deleted** from the Pandora item list/filter.
* ~~I'll write here about the odds soon~~

# Mystery Auction

Upon a "Mystery" auction finishing the bot will select **1** random item from Mystery filter/list then send the item name & image inside the text channel.

**Notes**
* The item the user won is **deleted** from the Mystery item list/filter


# Mystery Bundle Auction

Upon a "Mystery Bundle" auction finishing the bot will select **2 or more** items from the mbundle filter/list then send the item names & images inside the text channel.

**Notes**
* The items the user won are **deleted** from the mbundle item list/filter

# API (Nerdy shit)

### API [Endpoints] 

The following endpoints are available.

* **/failedtrades** (Returns a object of failed trades)
* **/trades** (Returns a object of **all** trades)
* **/thisauction** (Returns information about the **current** auction)
* **/auctions** (Creates auctions)

### API [failedtrades] **authorization required**
This endpoint is used to return a object of trades that failed to be sent.

### API [trades] **authorization required**
This endpoint is used to return a object of all trades in the database.

### API [thisauction] **authorization required**

This endpoint is used to get information about the current auction.

The values it returns are the following.
* **items** (Total items in this auction)
* **value** (The total value in **USD** of all items in the auction combined
* **bestItem** (Returns the name & price of the highest valued item)

### API [nextauction] **authorization required**

This endpoint is used to get information about the next auction.

The values it returns are the following.
* **timeleft** (Total time left until the next auction starts)
* **date** (The auction date/time)

> date is returned in GMT+8

### API [auction] **authorization required**
This endpoint is used to make the bot call the ``createAuctions()`` function.

After all the auctions have been created they are returned has a array object.