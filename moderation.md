# Waffles Helper Moderation Commands



### [Command] h!warn
**Usage: h!warn [@mention | UserID] [Reason]**

**Notes**

* Once a user gets **3** warnings they are automatically muted for **3 hours** 
* Once a user gets **3** warnings the past warnings are removed **automatically**

>[reason] is optional

### [Command] h!userwarns
**Usage: h!userwarns [@user | userid]**

Returns a list of the users **current** warnings.



### [Command] h!removewarn  
**Usage: h!removewarn [@user | userid] [caseID] [reason]**

Manually removes the warning from the user.


### [Command] h!mute
**Usage: h!mute [@user | userid] [hour(s)] [reason]**

Manually mutes the user for the provided [hour(s)].


### [Command] h!checkmute
**Usage: h!checkmute [@mention | UserID]**

Returns the length of time remaining until the user is unmuted.

### [Command] h!muteadd
**Usage: h!muteadd [@mention | UserID] [Hour(s)]**

Adds [Hours] onto the users current mute time.

### [Command] h!removemute
**Usage: h!removemute [@user | userid] [reason]**

Manually removes the users mute.
>[reason] is optional

### [Command] h!ban
**Usage: h!ban [@user | userid] [reason]**

Bans the user from the guild.

**Notes** 

* The bot also deletes all messages the user has sent in the last **24** hours.

### [Command] h!kick
**Usage: h!kick [@user | userid] [reason]**

Kicks the user from the guild.
