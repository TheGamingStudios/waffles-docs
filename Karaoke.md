# Hiff Helper Karaoke Commands [User]

### Important notes
**The system is designed to flow from singer to singer.**

**Only the current singer is able to speak in the voice channel whilst their song is playing**

### [Command] h!kjoin
**Usage: h!kjoin [url]**

Adds the user into the karaoke queue with the song **[url]**

**Notes**

* [url] must be a Youtube video.
* The song must be longer then 1 minute but no longer then 5 minutes.
* The user can **not** re-join the queue after they've sung.
* The user will be mentioned **10** seconds before their song will start.

![Image of queue](https://i.imgur.com/VsPgg89.pngg)
![Image of mention](https://i.imgur.com/IKxADxU.png)

### [Command] h!kmanage
**Usage: varies**

**h!kmanage song [url]** changes the song they're gonna be singing.

**h!kmanage ising** returns a message with the length of time until the user is singing.

**h!kmanage volume [value]** changes the volume of the bots broadcast.

**h!kmanage config** returns the current karaoke config.

**h!kmanage leave** removes you from the karaoke queue.

**Notes**

* Only the **current** singer & Bot Commanders can control the volume.
* Only "Developers" can edit the karaoke config
* If a user uses ``h!kmanage leave`` whilst their song is playing they'll be deducted -75 aegis.

### [Command] h!kqueue
**Usage: h!kqueue**

Returns an embed with the current users in queue to sing,

![Image of queue](https://i.imgur.com/aM4YwaS.png)


### [Command] h!kvote 
**Usage: h!kvote [vote]**

Votes [vote] for the **current** singer.

**notes** 
* [vote]  must be between 1 & 10
* You can't vote for yourself.
* You can't re-vote for a singer.
* You can't vote if you're **not** in the voice channel.

![Image of vote](https://i.imgur.com/CvcNyE9.png)


# Hiff Helper Karaoke Commands [Admin]

### [Command] h!kstart

**Usage: h!kstart**

Unlocks the karaoke text-channel followed by a mention that the event has just started.


### [Command] h!kend
**Usage: h!kend**

Ends the current karaoke event and rewards aegis followed by the end-karaoke embed.

Top 3 singers are paid **250** => **200** => **150** and everybody else gets **50**

![Imamge end embed](https://i.imgur.com/sZNSzCc.png)